create table board02(
	bno integer not null primary key,
	title varchar(200),
	content varchar(2000),
	writer varchar(200),
	viewcnt integer,
	show varchar(1),
	bgroup integer,
	sort integer,
	depth integer	
)
ALTER TABLE board02 add(regdate DATE default SYSDATE);
ALTER TABLE board02 RENAME COLUMN reddate TO regdate;
ALTER table board02 modify(show varchar(1) default 'y')


SELECT bno, title, content, writer, viewcnt, regdate, show ,bgroup, sort, depth FROM board02

------------------------------------------- 04/12

SELECT *
FROM BOARD02
ORDER BY BGROUP DESC, SORTS ASC

SELECT bno, bgroup, sort,depth, title, content, writer
FROM (SELECT ROWNUM RN , A.*
		FROM (SELECT bno, bgroup, sort, depth, title, content, writer FROM board02 ORDER BY bgroup DECS , depth ASC)A

		
--------------------------------------------- 04/13		

CREATE TABLE test01(
	BOARD NUMBER NOT NULL,
	BGROUP NUMBER NOT NULL,
	SORTS NUMBER NOT NULL,
	DEPTH NUMBER NOT NULL,
	SUBJECT VARCHAR(255)
)

INSERT INTO test01 VALUES ('1', '1', '0','0','1번글')
INSERT INTO test01 VALUES ('2', '2', '0','0','2번글')

SELECT * FROM test01 ORDER BY BGROUP DESC, SORTS ASC

SELECT NVL(MIN(SORTS),0) FROM test01
   WHERE  BGROUP = 2
   AND SORTS > 0
   AND DEPTH <= 0
--여기에서는 0이 나오게 된다.
   
SELECT NVL(MAX(SORTS),0) + 1 FROM test01 WHERE BGROUP = 2
--이곳에는 1이 나오는게 맞지! 그리고 이게 댓글이 될것이다.

INSERT INTO test01 VALUES (3, 2, 1, 1 ,'2번글-1')
-- 첫 댓글입력해서 넣기



SELECT NVL(MIN(SORTS),0) FROM test01
   WHERE  BGROUP = 2
   AND SORTS > 0
   AND DEPTH <= 0;
-- 0이 나온다.

SELECT NVL(MAX(SORTS),0) + 1 FROM test01 WHERE BGROUP = 2;
-- 2가 나온다. 그럼 이게 sort에 들어가는것인가 ?

INSERT INTO test01 VALUES (4, 2, 2, 1 ,'2번글-2');



SELECT NVL(MIN(SORTS),0)
FROM test01 WHERE  BGROUP = 2
AND SORTS > 1
AND DEPTH <= 1;
-- 2가 나올것이다. 흠.

UPDATE test01 SET SORTS = SORTS + 1 
WHERE BGROUP = 2  AND SORTS >= 2
-- 즉, 이전의 sort번호를 올려주고 새 답을글 밀어 넣는것이다.
-- 여기를 프로시져로 만들어서 해주면 될것 같은데.. 흠..  나중에 다시 시도해봐야지

INSERT INTO test01 VALUES (5, 2, 2, 2 ,'2번글-1-1')



CREATE OR REPLACE PROCEDURE update_reply (v_group IN number, v_sort IN number)
IS
BEGIN
	UPDATE test01 SET SORTS = SORTS + 1 WHERE BGROUP = v_group AND SORTS >= v_sort
	COMMIT;
END;
/
