<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시글</title>
</head>
<body>
<!-- 새 게시물을 작성하는건 나중에 만들어야겠다. -->
<!-- <a href="write.do">글 작성</a> -->

    <h2>게시글</h2>
    <table border="1" width="800px">
    <tr>
        <th width="400px">제목</th>
        <th>내용</th>
        <th>작성자</th>
        <th>작성일</th>
        <th>답글</th>
        
    </tr>
    <c:forEach var="row" items="${lists}">
    		<tr>
    				<td>
		    			<c:if test="${row.depth >0}">
			    			<c:forEach var="depth" begin="1" end= "${row.depth }" >
			    				&nbsp; &nbsp;
			    			</c:forEach>RE:
		    			</c:if>${row.bno}. ${row.title }
			       	</td>
			       	<td>${row.content }</td>
			        <td>${row.writer}</td>
			        <td>
			            <!-- 원하는 날짜형식으로 출력하기 위해 fmt태그 사용 -->
			            <fmt:formatDate value="${row.regdate}" pattern="YY-MM-dd"/>
			        </td>
			        <td>
			        	<a href="write.do?bno=${row.bno }&bgroup=${row.bgroup }&sort=${row.sort }&depth=${row.depth } ">답글달기</a>
			        </td>
		        
		    </tr>
    </c:forEach>
</body>
</html>
