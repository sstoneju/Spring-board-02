package com.spring.mvc.contorller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.mvc.HomeController;
import com.spring.mvc.dto.BoardVO;
import com.spring.mvc.service.BoardService;

@Controller
@RequestMapping("/board/*")
public class BoardController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	BoardService boardService;
	
	@RequestMapping(value="list.do")
	public String boardList(Model model){
		List<BoardVO> list = boardService.boardList();
		model.addAttribute("lists", list);
		
		logger.info("좋아 여기까지는 뜨는구나!! 나는 listAll");
		return "board/list";
	}
	
//	모델엔뷰를 이용한 컨트롤러
//	@RequestMapping(value="/")
//	public ModelAndView boardMain2(ModelAndView mav){
//		mav.addObject("list", boardService.boardList());
//		mav.setViewName("board/list");
//		return mav;
//	}
//		
	
	@RequestMapping("view.do")
	public String boardDetail(@RequestParam int bno, Model model) {
		model.addAttribute("dto", boardService.boardDetail(bno));
		return "board/view";
	}
	
	@RequestMapping(value="write.do", method=RequestMethod.GET)
	public String boardWrite(@ModelAttribute BoardVO vo)
	{
		return "board/write";
	}
	
	@RequestMapping(value ="insert.do", method = RequestMethod.POST)
	public String boardInsert(Model model, @ModelAttribute BoardVO vo) {
		//logger.info(bno+"뜨는가!!");
		BoardVO DVO = boardService.boardDetail(vo.getBno());
		int Rebno = DVO.getBno();
		int sort = DVO.getSort();
		int depth = DVO.getDepth();
		vo.setBgroup(Rebno);
		vo.setSort(sort+1);
		vo.setDepth(depth+1);
		
		boardService.boardInsert(vo);
		return "redirect:list.do";
	}
	
	@RequestMapping("delete.do")
	public String boardDelete(@RequestParam int bno) {
		boardService.boardDelete(bno);
		return "redirect:list.do";
	}
	
}
