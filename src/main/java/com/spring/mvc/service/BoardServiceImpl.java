package com.spring.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mvc.dao.BoardDAO;
import com.spring.mvc.dto.BoardVO;

@Service
public class BoardServiceImpl implements BoardService{
	
	@Autowired
	BoardDAO boardDao;
	
	@Override
	public List<BoardVO> boardList() {
		return boardDao.boardList();
	}

	@Override
	public BoardVO boardDetail(int bno) {
		return boardDao.boardDetail(bno);
	}

	@Override
	public void boardInsert(BoardVO vo) {
		boardDao.boardInsert(vo);
	}

	@Override
	public void boardUpdate(BoardVO vo) {
		boardDao.boardUpdate(vo);
	}

	@Override
	public void boardDelete(int bno) {
		boardDao.boardDelete(bno);
	}
	
}
