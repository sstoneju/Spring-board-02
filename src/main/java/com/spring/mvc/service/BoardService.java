package com.spring.mvc.service;

import java.util.List;

import com.spring.mvc.dto.BoardVO;

public interface BoardService {
	public List<BoardVO> boardList();
	public BoardVO boardDetail(int bno);
	public void boardInsert(BoardVO vo);
	public void boardUpdate(BoardVO vo);
	public void boardDelete(int bno);
}
