package com.spring.mvc.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mvc.dto.BoardVO;

@Repository
public class BoardDAOImpl implements BoardDAO{
	
	@Autowired
	SqlSession sqlSession;
	
	@Override
	public List<BoardVO> boardList() {
		return sqlSession.selectList("board.listAll");
	}

	@Override
	public BoardVO boardDetail(int bno) {
		return sqlSession.selectOne("board.detail", bno);
	}

	@Override
	public void boardInsert(BoardVO vo) {
		sqlSession.insert("board.insert", vo);
	}

	@Override
	public void boardUpdate(BoardVO vo) {
		sqlSession.update("board.update", vo);
		
	}

	@Override
	public void boardDelete(int bno) {
		sqlSession.delete("boarddd.delete", bno);
		
	}

}
